# React-ToDosApp

**Exercise using npm, webpack, react and ES6**
Source: https://www.youtube.com/watch?v=IR6smI_YJDE

This app will help you to organize your daily tasks! 

## How to use it?
Here comes **webpack**. Clone or download the project on your computer and use your terminal to run it. First go to **downloaded/cloned** folder and use command `npm install` and `npm install -g webpack webpack-dev-server`(if you do not have it already installed on your machine) Next run command `webpack-dev-server`and go to localhost 8080. This will get you running the project!
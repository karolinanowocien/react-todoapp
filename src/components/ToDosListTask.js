import React from 'react';
require('styles/main.scss');

export default class ToDosListTask extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    const { task, isCompleted } = this.props;
    const taskStyle = {
      color:'#333',
      fontWeight: isCompleted ? '100' : '600',
      textTransform: 'uppercase',
      fontSize: '12px',
      cursor: 'pointer',
      textDecoration: isCompleted ? 'line-through' : 'none'
    };

    if (this.props.state=='isEditing') {
      return(
        <td className="todos__task">
          <input className="table__input" type="text" ref="editInput"
           onChange={this.props.onTaskEdit.bind(this)}
           value={this.props.editingTask}
           placeholder={'Please enter a valid task.'} />
        </td>
      );
    }

    return (
      <td  className="todos__task" style={taskStyle}
      onClick={this.props.toggleTask.bind(this, task)}
      >
        {task}
      </td>
    );
  }

}

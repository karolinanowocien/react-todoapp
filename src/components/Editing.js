import React from 'react';

export default class Editing extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      return(
        <td className="table__buttons">
          <button className='button button__left' onClick={this.props.onSaveClick.bind(this, this.props.task)}>Save</button>
          <button className='button button__right' onClick={this.props.onCancelClick.bind(this, this.props.task)}>Cancel</button>
        </td>
      );
  }

}

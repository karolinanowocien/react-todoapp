import React from 'react';


export default class Displaying extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      console.log('Displaying', this.props)
      const { props } = this.props;
      return(
        <td className="table__buttons">
          <button className='button button__left' onClick={this.props.onEditClick.bind(this, this.props.task)}>Edit</button>
          <button className='button button__right' onClick={this.props.deleteTask.bind(this, this.props.task)}>Delete</button>
        </td>
      );
  }

}

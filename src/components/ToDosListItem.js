import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import ToDosListTask from './ToDosListTask';
import ToDosListButton from './ToDosListButton';
require('styles/main.scss');
require('styles/date-picker/datepicker.scss');

export default class ToDosListItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editingTask: props.task,
      error: null
    }
  }

  onCancelClick(task) {
    this.setState({error: null});
    this.props.onCancelClick(task);
  }

  onTaskEdit(event) {
    this.setState({editingTask: event.target.value});
  }

  onTaskSave(task, event) {
    event.preventDefault();
    const validateTask = this.props.validateTask(this.props.task, this.state.editingTask);

    this.setState({error: null})

    if(validateTask === null) {
      this.props.saveTask(this.props.task, this.state.editingTask);
      this.props.taskCompletion(this.props.isCompleted, this.state.editingTask);
    }

    if(validateTask) {
      this.setState({ error: validateTask });
      return;
    }
  }

  renderError(){
    if (!this.state.error) { return null; }
    return <div className='form_error'>{this.state.error}</div>;
  }

  render() {
    return (
      <tbody>
        <tr className="table__row">
          <ToDosListTask
          task={this.props.task}
          state={this.props.state}
          isCompleted={this.props.isCompleted}
          toggleTask={this.props.toggleTask.bind(this)}
          editingTask={this.state.editingTask}
          onTaskEdit={this.onTaskEdit.bind(this)}
          />
          <td className="date__wrapper">
          <DatePicker
           date={this.props.dueDate}
           dateFormat="YYYY/MM/DD"
           selected={this.props.dueDate}
           onChange={(date) => this.props.handleDateChange(date, this.props.index)}
           minDate={moment()}
           placeholderText="Deadline"
           isClearable={true}
           />
           </td>
          <ToDosListButton
          task={this.props.task}
          state={this.props.state}
          ///Displaying////////////////////////////////
          deleteTask={this.props.deleteTask.bind(this)}
          onEditClick={this.props.onEditClick.bind(this)}
          ///Editing////////////////////////////////
          saveTask={this.props.saveTask.bind(this)}
          onSaveClick={this.onTaskSave.bind(this)}
          onCancelClick={(task) => this.onCancelClick(task)}
          ///onHold////////////////////////////////
          onDisplayClick={this.props.onDisplayClick.bind(this)}
           />
        </tr>
        <tr className="table__row--error">
           <td className="table__error">{this.renderError()}</td>
        </tr>
      </tbody>
    );
  }

}

import React from 'react';
import OnHold from './OnHold';
import Editing from './Editing';
import Displaying  from './Displaying';
require('styles/main.scss');


export default class ToDosListButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { props } = this.props;

    if(this.props.state=='isOnHold') {
      return(
        <OnHold
        task={this.props.task}
        state={this.props.state}
        onDisplayClick={this.props.onDisplayClick.bind(this)}
        />
      );
    }
    else if(this.props.state=='isEditing') {
      return(
        <Editing
        task={this.props.task}
        state={this.props.state}
        saveTask={this.props.saveTask.bind(this)}
        onSaveClick={this.props.onSaveClick.bind(this)}
        onCancelClick={this.props.onCancelClick.bind(this)}
         />
      );
    }
      return (
        <Displaying
        task={this.props.task}
        state={this.props.state}
        deleteTask={this.props.deleteTask.bind(this)}
        onEditClick={this.props.onEditClick.bind(this)}
        />
      );
    }

}

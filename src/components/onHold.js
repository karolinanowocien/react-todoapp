import React from 'react';

export default class OnHold extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
      return(
        <td className="table__buttons">
           <button className='header__link' onClick={this.props.onDisplayClick.bind(this, this.props.task)}>
             <img className='header__img--edit' src='src/img/edit.png' />
           </button>
       </td>
      );
  }

}

import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import CreateToDo from './CreateToDo';
import ToDosList from './ToDosList';
require('styles/main.scss');
//Sample data////////
const todos = [
  {
    task: 'learn react',
    isCompleted: false,
    state: 'isOnHold',
    dueDate: null,
  },
  {
    task: 'eat healthier',
    isCompleted: true,
    state: 'isOnHold',
    dueDate: null
  },
  {
    task: 'buy a gold fish',
    isCompleted: false,
    state: 'isOnHold',
    dueDate: null
  },
  {
    task: 'buy vegetables',
    isCompleted: false,
    state: 'isOnHold',
    dueDate: null
  },
  {
    task: 'read the article',
    isCompleted: true,
    state: 'isOnHold',
    dueDate: null
  },
  {
    task: 'make a cup of tea',
    isCompleted: true,
    state: 'isOnHold',
    dueDate: null
  }
];
/////////////////////////

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todos,
      editingTask: this.task
    };
  }

  render() {
    return (
      <div className='app__container'>
        <header>
          <div className='header__container'>
              <h1>Todo list:</h1>
          </div>
        </header>
          <CreateToDo todos={todos} createTask={this.createTask.bind(this)} />
          <ToDosList
            root={this}
            todos={todos}
            toggleTask={this.toggleTask.bind(this)}
            saveTask={this.saveTask.bind(this)}
            validateTask={this.validateTask.bind(this)}
            taskCompletion={this.taskCompletion.bind(this)}
            // onSaveClick={this.onSaveClick.bind(this)}
            onCancelClick={this.onCancelClick.bind(this)}
            deleteTask={this.deleteTask.bind(this)}
            onDisplayClick={this.onDisplayClick.bind(this)}
            onEditClick={this.onEditClick.bind(this)}
            editingTask={this.state.editingTask}
            handleDateChange={this.handleDateChange.bind(this)}
           />
      </div>
    );
  }

  handleDateChange(date, index) {
    todos[index].dueDate = date;
    this.setState({ todos });
  }

  toggleTask(task) {
    const foundToDo = _.find(todos, todo => todo.task === task);
    foundToDo.isCompleted = !foundToDo.isCompleted;
    this.setState({ todos });
  }

  createTask(task) {
    todos.push({
      task,
      isCompleted: false,
      state: 'isOnHold',
      dueDate: null
    });
    this.setState({ todos });
  }

  validateTask(task, editedTask) {
    if (!editedTask){
      return 'Please, enter a task!';
    } else if (_.find(todos, todo =>
        todo.task.toLowerCase() === editedTask.toLowerCase() &&
        task.toLowerCase() != editedTask.toLowerCase())
    ) {
      return 'Task already exists.';
      this.setState({editingTask: ''});
    } else {
      return null;
    }
  }

  deleteTask(taskToDelete) {
    _.remove(todos, todo =>todo.task === taskToDelete);
    this.setState({ todos });
  }

  saveTask(oldTask, newTask) {
    const foundToDo = _.find(todos, todo => todo.task === oldTask);
    foundToDo.task = newTask;
    this.setState({ todos });

    for(var i=0; i < todos.length; i++){
       if(todos[i].task == newTask){
       todos[i].state = 'isOnHold'
       }
     }
  }

  taskCompletion(isCompleted, editedTask) {
    for(var i=0; i < todos.length; i++){
      if(todos[i].task == editedTask){
      todos[i].isCompleted = false
      }
    }
    this.setState({ todos });
  }

  //////////////Events//////////////////////
  onDisplayClick(task) {
    for(var i=0; i < todos.length; i++){
      if(todos[i].task == task){
      todos[i].state = 'isDisplaying'
      } else {
        todos[i].state = 'isOnHold';
      }
    }
    this.setState({ todos });
  }
  onEditClick(task) {
    for(var i=0; i < todos.length; i++){
      if(todos[i].task == task){
        todos[i].state = 'isEditing';
      }
    }
    this.setState({ todos });
  }
  onCancelClick(task) {
    for(var i=0; i < todos.length; i++){
      if(todos[i].task == task){
        todos[i].state = 'isOnHold';
      }
    }
    this.setState({ todos });
  }
  // onSaveClick(task) {
  //
  //   for(var i=0; i < todos.length; i++){
  //     if(todos[i].task == task){
  //     todos[i].state = 'isOnHold';
  //     }
  //   }
  //   this.props.saveTask(oldTask, newTask);
  //   this.setState({ todos });
  // }

}

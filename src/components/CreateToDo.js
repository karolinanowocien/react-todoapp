import React from 'react';
require('styles/main.scss');


export default class CreateToDo extends React.Component {
  ///Should go to app.js////////
  constructor(props) {
    super(props);

    this.state = {
      error: null
    };
  }
  ///////////////////
  renderError(){
    if (!this.state.error) { return null; }
    return <div className='form_error'>{this.state.error}</div>;
  }
  render() {
    return (
      <form className='form' onSubmit={this.handleCreate.bind(this)}>
        <h3 className='form__header'>My new task will be…</h3>
        <div className='form__container'>
          <input className='form__input' type="text" placeholder="What do I need to do?" ref="createInput"  />
          <button className='form__button'>Create</button>
        </div>
        {this.renderError()}
      </form>
    );
  }

  handleCreate(event) {
    event.preventDefault();

    const createInput = this.refs.createInput;
    const task = createInput.value;
    const validateInput = this.validateInput(task);

    if(validateInput) {
      this.setState({ error: validateInput });
      return;
    }

    this.setState({ error: null });
    this.props.createTask(task);
    this.refs.createInput.value = '';
  }

  validateInput(task) {
    if (!task){
      return 'Please, enter a task!';
    } else if (_.find(this.props.todos, todo => todo.task.toLowerCase() === task.toLowerCase())){
      return 'Task already exists.';
    }else{
      return null;
    }

  }
}

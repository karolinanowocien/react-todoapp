import _ from 'lodash';
import React from 'react';
import ToDosListItem from './ToDosListItem';

export default class ToDosList extends React.Component {
  constructor(props) {
    super(props);
  }

  renderItems() {
    const props = _.omit(this.props, 'todos');
    return _.map(this.props.todos, (todo, index) =>
      <ToDosListItem key={index} index={index} {...todo} {...props} />);
  }
  render() {
    return (
      <table>
          {this.renderItems()}
      </table>
    );
  }
}
